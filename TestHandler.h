﻿#pragma once
#include <string>
#include <vector>

using namespace std;

class TestHandler
{
private:
    
    const string AnswersPath = "./1.Tickets";

    vector<long long> AnswerNumbersData;
    vector<int> TestNumbersData;
    
    
public:
    
    vector<int> NeedTestNumbers() {return TestNumbersData;}
    bool CompareAnswers(int TestNumber, long long GotAnswer);
    
    void GetDataFromFiles();
    bool IsFileNumeric(string FileContent);
    bool IsFileAnswer(string FilePat);
    void PrintResult(int TestNumber, bool Result);
};
