﻿#include "TestHandler.h"

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING 1;
#include <experimental/filesystem>

using namespace std;
namespace FileSystem = experimental::filesystem;

void TestHandler::GetDataFromFiles()
{
    for (const auto& File : FileSystem::directory_iterator(AnswersPath))
    {
        const string FullFilePath = File.path().generic_string();
        ifstream CurrentFile(FullFilePath);
        
        if (!CurrentFile.is_open())
        {
           continue;
        }

        string FileContent; 
        getline(CurrentFile, FileContent, '\n');
        
        if (!IsFileNumeric(FileContent))
        {
            continue;
        }
        
        IsFileAnswer(FullFilePath) ? AnswerNumbersData.push_back(stoll(FileContent)) : TestNumbersData.push_back(stoi(FileContent));
    }
}

bool TestHandler::IsFileNumeric(string FileContent)
{
    for (char Symbol : FileContent)
    {
        if (Symbol < 0)
        {
            return false;
        }
        if (!isdigit(Symbol))
        {
            return false;
        }
    }
    
    return true;
}

bool TestHandler::IsFileAnswer(string FilePath)
{
    int LastIndex = static_cast<int>(FilePath.length()) - 1;
    char EndingOfAnswerFile = 't';
    
    return EndingOfAnswerFile == FilePath[LastIndex];
}

bool TestHandler::CompareAnswers(int TestNumber, long long GotAnswer)
{
    //start with 0 
    int Index = TestNumber - 1;
    return AnswerNumbersData[Index] == GotAnswer;
}

void TestHandler::PrintResult(int TestNumber, bool Result)
{
    string LiteralResult = Result ? "Success" : "Failure";
    cout << "Test " << TestNumber << " is " <<  LiteralResult << endl;
}