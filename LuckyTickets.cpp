#include <iostream>
#include <vector>

#include "TestHandler.h"

using namespace std;

#define MaxDigit 9;

vector<long long> FillArrayWithNextRow(vector<long long> OriginalArray)
{
    const unsigned int ArraySize = static_cast<unsigned int>(OriginalArray.size());
    int NewArrayLength = ArraySize + MaxDigit;

    vector<long long> Array;

    for (int i = 0; i < NewArrayLength; i++)
    {
        long long Value = 0;
        for (int j = 0; j < 10; j++)
        {
            unsigned int Difference = i - j;
            if (Difference < ArraySize && OriginalArray.at(Difference))
            {
                Value += OriginalArray.at(Difference);
            }
        }

        Array.push_back(Value);
    }
    return Array;
}

long long FindCountOfLuckyTickets(int NumberOfTicketDigits)
{
    vector<long long> Array(10, 1);

    for (int i = 0; i < NumberOfTicketDigits / 2 - 1; i++)
    {
        Array = FillArrayWithNextRow(Array);
    }
    long long NumberOfTickets = 0;

    for (long long Element : Array)
    {
        NumberOfTickets += static_cast<long long>(pow(Element, 2));
    }
    return NumberOfTickets;
}


int main(int argc, char* argv[])
{
    TestHandler* Test_Handler = new TestHandler;
    Test_Handler->GetDataFromFiles();
    
    for (int TestNumber : Test_Handler->NeedTestNumbers())
    {
        int FirstPartOfQuantityDititsOfTicketNumber = TestNumber * 2;
        long long CountOfLickyTickets = FindCountOfLuckyTickets(FirstPartOfQuantityDititsOfTicketNumber);
        
        bool Result = Test_Handler->CompareAnswers(TestNumber, CountOfLickyTickets);
        Test_Handler->PrintResult(TestNumber, Result);
    }
    
    system("pause");
    return 0;
}
